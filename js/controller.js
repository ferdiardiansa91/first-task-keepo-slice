var myApp = angular.module('myApp', ['infinite-scroll','ngSanitize']);

myApp.controller('infinityScrollerController', function($scope, $http) {
    $scope.feeds  	  	= [];
    $scope.content		= [];
    $scope.Currentpage	= 0;
    $scope.totalPage 	= 25;
    var temp_data 		= [];
    $scope.numLimit		= 77;
	
	$scope.readMore=function(){
		$scope.numLimit = 75;
	};

    function getData (page) {
    	$scope.isLoading = true;
        $http.get("json/feeds.json").then(function(response) {
        	angular.forEach(response.data, function(value) {
        		for(var i = 0; i < value.length; i++) {
                    $scope.feeds.push(value[i]);
        		}
        	});
        	$scope.isLoading = false;
        }, function() {
        	$scope.isLoading = false;
        });
    }

    getData($scope.Currentpage);

    $scope.loadMore = function () {
    	if($scope.Currentpage < $scope.totalPage) {
    		$scope.Currentpage += 1;
    		getData($scope.Currentpage);
    	}
    }
});

myApp.directive("infinityscroll", function () {
    return {
    	restrsict : 'A',
    	link : function (scope, element, attrs) {
            element.bind("scroll", function() {
                if ((element[0].scrollTop + element[0].offsetHeight ) == element[0].scrollHeight) {
                    scope.$apply(attrs.scroll);
                }
            });
        }
    }
});
