$(document).ready(function() {

			$('#icon-bar-responsive a').on('click', function() {
				$('nav').fadeToggle(200);
				$(this).toggleClass('btn-open').toggleClass('btn-close');
			});

		});

		$(window).load(function(){
			$('#loader').fadeOut(800);

			setTimeout(function() {
				$('#main-container .card').each(function(i) {
					setTimeout(function() {
						$('.card').eq(i).addClass('is-showing');
					}, 150 * (i+1));
				});
			}, 30);

			setTimeout(function() {
				$('#main-container .ad').each(function(i) {
					setTimeout(function() {
						$('.ad').eq(i).addClass('is-showing');
					}, 450 * (i+1));
				})
			},10);
		}); 

		$(window).scroll(function() {
			var wScroll 	= $(this).scrollTop();
			// console.log(wScroll);
			if(wScroll > 300){
				$('.main-header').addClass('fixed');
			}else{
				$('.main-header').removeClass('fixed');
			}

			setTimeout(function() {
				$('#main-container .card').each(function(i) {
					setTimeout(function() {
						$('.card').eq(i).addClass('is-showing');
					}, 20 * (i+1));
				});
			}, 20);

			setTimeout(function() {
				$('#main-container .ad').each(function(i) {
					setTimeout(function() {
						$('.ad').eq(i).addClass('is-showing');
					}, 20 * (i+1));
				})
			},10);

		})